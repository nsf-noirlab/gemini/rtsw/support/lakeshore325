#!../../bin/linux-x86_64/lab

## You may have to change lab to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/ls336.dbd"
lab_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadTemplate "db/userHost.substitutions"
#dbLoadRecords "db/dbSubExample.db", "user=mrippaHost"

## Set this to see messages from mySub
#var mySubDebug 1
var streamDebug 1

## Run this to trace the stages of iocInit
traceIocInit

# Lakeshore 336 Temperature Controller 
#drvAsynIPPortConfigure("LS336", "10.1.2.18:7777", 0, 0, 0)
drvAsynIPPortConfigure("LS336", "10.1.2.18:7777")
asynOctetSetInputEos("LS336",0,"\n")
asynOctetSetOutputEos("LS336",0,"\n")

epicsEnvSet ("STREAM_PROTOCOL_PATH", "/home/mrippa/root/src/GEM10/epics_modules-lakeshore336/ls336App/Db")
#dbLoadRecords("db/LakeShore336.db", "P=xxx:,Q=TC1,PORT=LS336_IP,ADDR=0")
dbLoadRecords("db/LakeShore336.db", "P=gm,Q=TC1,PORT=LS336,ADDR=0")



cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncExample, "user=mrippaHost"
